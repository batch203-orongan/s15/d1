console.log("Hello, world!");
console.log ("Hello, Batch 203!")
console.
log
(
	"Hello, everyone!"
)
// comments: 

// This is a single line comment. ctrl + /
/*
	This is a 
	Multi line comment
	ctrl + shft + /
*/


// Syntax and Statements

// Statements in programming are instructions that we tell the computer to perform

// Syntax in programming, it is the set of rules that describes how statements must be constructed. or the correct order of statements.

// Variables
/*
	-It is used to contain data

	-Syntax in declaring variables
		- let/const variableNameSample;		
*/

let myVariable = "Hello";

console.log(myVariable);

// console.log(hello);

// let hello;

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign the value.
		2. Variable names should start with lowercase character, use camelCase for multiple words.
		3. For conststn variable, use the 'const' keyword.
		4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
		5. Never name a variable starting with numbers.
*/

/* Declaring a variable with initial value
	let/const varibleName = value
*/

let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// let - we usually change/reassign the value in our variable.

// Reassigning variable values
// Syntax
	// variableName = newValue

productName = "Laptop";
console.log(productName)

let friend = "Kate";
friend = "Jane";
console.log(friend); // error: Identifier "friend" has already been declared.

// interest = 4.489;
console.log(interest); // error because of asssigning a value to a constant variable.

// Reaassigning variables vs Initializing variables.
// Declare ng variable
let supplier;
// Initialization is done after the variable has been declared
supplier = "John Smith Tradings";
console.log(supplier);

// Reassignment if variable because it's initial value was already declared.
supplier = "Zuitt Store";
console.log(supplier)

const pi = 3.1416;
// pi = 3.1416
console.log(pi); // error due to const initialization.

// var(ES1) vs let/const(ES6)
// let/const keyword avoid the hosting of variables.
// a = 5
// console.log(a);
// var a;

// Multiple variable declerations
let productCode = "DC017"; 
const productBrand = "Dell";
console.log(productCode, productBrand);

// using a variable with a reserved keyword.
// const let ="hello";
// console.log(let); // error: cannot used reserved keyword as a variable name.

// [SECTION] Data Types

// Strings
// Strings are series of characters that creates a word, a phrase, a sentence or anything related to creating text.
// Strings in JavaScript a single (' ') or double ("") quote.
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings 
// Multiple string calues can be combined to create a single strings using the "+" symbol.
let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting ="I live in the " + country;
console.log(greeting);

// Escape characters(\)
// "\n" refers to creating a new line or set the text to next line.
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine number and strings
console.log("John's grade last quarter is 98.7");

// Boolean
// true/false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// it is used to store multiple calues with similar data type.
// Syntax:
	// let/const arrayName = [elemantA, elementB, elementC, .......]
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	// different data types
	// Storing different data types inside an array is not recommended because it will not make sense in the context of programming.
	let details = ["John", "Smith", 32, true];
	console.log(details);

// Objects
// Objects are another special kind of data type that is used to mimic real world objects/items.
// Syntax
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person);

	// Create Abstract Object
	let myGrade = {
		firstGrading: 98.7,
		secondGraddig: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6,
	}
	console.log(myGrade);

	// typeof operator is used ti determine the typw of data or variable value.
	console.log(typeof myGrade);

	console.log(typeof grades);

	// Constant Objects and Arrays
	// We cannot assign the value of the variable but we can change the elements of the constant array.
	const anime = ["one piece", "one punc man", "attack on titans"];
	console.log(anime);
	anime[0] = "kimetsu no yaiba";

	console.log(anime);

	// Null
	// It is used to intentionally express the abscence of the value in a variable decleration/initialization.
	let spouse = null;
	console.log(spouse);

	let myNumber = 0; // number
	let myString = ""; // string

	// Undefined
	// Represents the state if a variable that has been declared but without an assigned value.
	let fullname;
	console.log(fullname);
	
